import React, { Fragment } from "react";
import Head from "next/head";

import Navigation from "./navigation";

const Container = ({ children, titulo }) => {
  return (
    <Fragment>
      <Head>
        <title>Hola mundo Nextjs - {titulo}</title>
        <link
          rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/flatly/bootstrap.min.css"
        ></link>
        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
          crossorigin="anonymous"
        ></script>
      </Head>
      <Navigation />
      <div className="container p-4">{children}</div>
    </Fragment>
  );
};

export default Container;
